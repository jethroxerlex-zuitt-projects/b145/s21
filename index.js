//ES6 Updates
//Math.pow() -> return the base to the exponent power, as in base^exponent
//before update
console.log(7 ** 3);
//after update
console.log(Math.pow(7,3)); 


//Template Literals (``) backticks
	//placeholders ${}
	//Allows to write string without using the concatenation operator
let from = 'ms Thonie'

let message = 'Hello Batch 145 love ' + from;
console.log(message);

//how to create multi-line strings?
console.log('Hi sirs and mam \n Hello sirs and mam')

console.log(`Hello Batch 145 love, ${from}`);
console.log(`
Hello
Hello
Hello
Hello
	`)
//with ebedded JS expressions
const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);
let name = 'John';
//before
let message2 = 'Hello ' + name + '! Welcome to programming!'
console.log('message without template literals: ' + message2);
//using template literals
message2 = `Hello ${name}! Welcome to programming!`;
console.log(`message with template literals: ${message2}`);

//[DESTRUCTURE ARRAYS AND OBJECTS USING ES6 METHODS]
//based on  the position of elements

let grades = [89,87,78,96];
let fullName = ['Juan', 'Dela','Cruz'];

//Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);

//Array Destructuring
let [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

function addAndSubtract(a,b) {
	return [a+b, a-b];
}

const array = addAndSubtract(4, 2);
console.log(array);

const [sum, difference] = addAndSubtract(4,2)
console.log(sum);
console.log(difference);

const numbers = [1,2,3,4,5]
const letters = ['A','B','C','D','E'];
const [a, ...rest] = letters; //'...' -> spread operator
console.log(a);
console.log(rest);



//4.DESTRUCTURE OBJECTS
console.warn('Object Destructuring:');
//-allows to unpack properties of an object into distinct variables
//- based on the name of the key

const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
};

//before
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello, my name is ${person.givenName} ${person.maidenName} ${person.familyName}!`);
//object destructuring
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello my name is ${givenName} ${maidenName} ${familyName}!`);


// function printUser(user){
// 	console.log(user);
// }

// function printUser(givenName,familyName){
// 	console.log(`I am ${givenName} ${familyName}`);
// };
// printUser(person);

// const person2 = {
// 	"given Name": 'James',
// 	"family Name": 'Bond'}
// 	console.log(person2["given name"]);

const person3 = {
	fname: 'Jill',
	age: 27,
	email:'jill@mail.com',
	address: {
		city: 'Some city',
		street: 'Some street'
	}
};

const {fname , age, email = null} = person3;
console.log(fname);
console.log(age);
console.log(email);


//5.ARROW FUNCTIONS
console.warn('Arrow Functions:');
//Syntax:
//const/let variableName = (parameters) =>{statements};

//before

const hello = function(){
	console.log("Hello world!");
}

//using arrow function

const helloAgain = () => {
	console.log("Hello world!");
}

hello();
helloAgain();

//before
function printFullName(firstName,middleInitial,lastName) {
	console.log(firstName+' '+ middleInitial + '. ' + lastName);
}
printFullName('John','D','Smith');

//using arrow function

const printFullName2 = (firstName,middleInitial,lastName) => {
	console.log(`${firstName} ${middleInitial}. ${lastName}`);
}
 
 printFullName2('John','D','Smith');

 //before
 let numberMap = numbers.map(function(number){
 	return number * number;
 })
console.log(numberMap)

//using arrow function
let numberMap2 = numbers.map((number) =>{
	return number * number
});

console.log(numberMap2);

//example 3
 let numberFilter = numbers.filter(function(number){
 	return number > 2;
 })
 console.log(numberFilter);
 //using arrow function
  let numberFilter2 = numbers.filter((number) =>{
 	return number > 2;})
 	console.log(numberFilter2)

//Arrow Functions using loops
const students = ['Joy','Jade','Judy'];

//before
students.forEach(function(student){
	console.log(`${student} is a student.`);
})

//using arrow function

students.forEach((student) =>{
	console.log(`${student} is a student.`)
})


//6. =>IMPLICIT RETURN STATEMENT
console.warn('Implicit Return Statement:');
//const add = (x,y) => {return x + y};
const add = (x,y) => x + y;
let total = add(1,2);
console.log(total);

//7. DEFAULT FUNCTION ARGUMENT VALUE
console.warn('Default Function Argument Value:')

const greet = (name = "User") => {
	return `Good morning, ${name}`;
}

console.log(greet('Jake'));
console.log(greet());

//8.CLASS-BASED OBJECT BLUEPRINTS
console.warn('Class-Based Object Blueprints:');
//- allows creation of objects using classes as blueprints
//- the constructor is a special method of a class for creating an object for that class.
/*syntax
class className {
	constructor(propertyA,propertyB) {
	this.propertyA = propertyA;
	this.propertyB = propertyB;
	}
}
*/

class Car {
	consturctor(brand,name,year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

//creating a new instance of car object
const myCar = new Car();
console.log(myCar);

//values of properties may be assigned after creation of an object.
myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;
console.log(myCar);

//creating new instance of car with initialized values

const cars = new Car('Toyota', 'Vios', 2021);

console.log(cars);




let fruits = ["mango", "pear", "banana"];
let mappedFruits = fruits.map(fruit => "banana");
console.log(mappedFruits[2]);